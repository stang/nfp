import re

from .data_access import call_commons_api
from .user import build_user_library

class Image(object):
    def __init__(self) -> None:
        self.title = ''
        self.file_url = ''
        self.thumb_url = ''
        self.large_thumb_url = ''
        self.user = None
        self.desc_content = ''
        self.license = 'Unknown'
        self.source = 'Unknown'
        self.rcid = None

    def parse_license(self):
        sources = re.findall(r'\| *?[sS]ource *?=\s*(.+)', self.desc_content)
        if len(sources) == 1:
            self.source = sources[0]
        licenses = re.findall(r'== *?\{\{int:license-header\}\} *?==\n(.+)', self.desc_content)
        if len(licenses) == 1:
            self.license = licenses[0]
def add_image_license(images):
    titles = ['File:' + i.title for i in images]
    params = {
        'action': 'query',
        'titles': '|'.join(titles),
        'prop': 'revisions',
        'rvslots': 'main',
        'rvprop': 'content'
    }
    res = call_commons_api(params)
    for page_id in res['query']['pages']:
        page = res['query']['pages'][page_id]
        title = page['title'][len('File:'):]
        content = None
        for i in page.get('revisions', []):
            content = i['slots']['main']['*']
        if not content:
            continue
        for image in images:
            if image.title == title:
                image.desc_content = content 
                image.parse_license()

def build_image_data(images):
    titles = set([i['title'] for i in images])

    params = {
        'action': 'query',
        'iiurlwidth': 120,
        'titles': '|'.join(['File:' + i for i in titles]),
        'prop': 'imageinfo',
        'iiprop': 'url'
    }
    res = call_commons_api(params)
    images_library = {}
    for page_id in res['query']['pages']:
        try:
            title = res['query']['pages'][page_id]['title'][len('File:'):]
            file_url = res['query']['pages'][page_id]['imageinfo'][0]['url']
            thumb_url = res['query']['pages'][page_id]['imageinfo'][0]['thumburl']
            large_thumb_url = res['query']['pages'][page_id]['imageinfo'][0]['responsiveUrls']["2"]
        except:
            continue
        image = Image()
        image.title = title
        image.file_url = file_url
        image.thumb_url = thumb_url
        image.large_thumb_url = large_thumb_url
        image.rcid = [img for img in images if img['title'] == title][0]['rcid']
        images_library[title] = image

    users_library = build_user_library(set([i['user'] for i in images]))

    new_images = []
    for i in images:
        if i['title'] not in images_library or i['user'] not in users_library:
            continue
        image = images_library[i['title']]
        image.user = users_library[i['user']]
        new_images.append(image)
    add_image_license(new_images)
    return new_images