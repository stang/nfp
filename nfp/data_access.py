import requests
import toolforge

def call_commons_api(params):
    params['format'] = 'json'
    return requests.get('https://commons.wikimedia.org/w/api.php', params=params).json()

def get_most_recent_unpatrolled_images(wiki, from_rc_id=None):
    images = []
    conn = toolforge.connect(wiki)
    conds = [
        'rc_patrolled = 0',
        'rc_last_oldid = 0',
        'rc_namespace = 6',
        'rc_type = 3',
        "rc_source = 'mw.log'",
    ]
    if from_rc_id and isinstance(from_rc_id, int):
        conds.append('rc_id < ' + str(from_rc_id))
    sql = "select STRAIGHT_JOIN rc_title, actor_name, rc_id from recentchanges join actor on rc_actor = actor_id where " + ' AND '.join(conds) + " order by rc_timestamp desc limit 50;"
    rc_ids = []
    with conn.cursor() as cur:
        cur.execute(sql)
        for row in cur.fetchall():
            images.append({'title': row[0].decode('utf-8').replace('_', ' '), 'user': row[1].decode('utf-8'), 'rcid': row[2]})
            rc_ids.append(int(row[2]))
    return images, min(rc_ids)