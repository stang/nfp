from .data_access import call_commons_api
from datetime import datetime, timedelta
from datetime import date

class User(object):
    def __init__(self) -> None:
        self.user_name = ''
        self.edit_count = 0
        self.formatted_edit_count = ''
        self.blocked = False
        self.groups = ''
        self.registration = ''
        self.home_wiki = ''
        self.global_groups = ''
        self.globally_locked = False
        self.blocked_in_wikis = ''
        self.global_edit_count = 0
        self.formatted_global_edit_count = ''

    def set_age_from_registration(self):
        if not self.registration:
            self.age = 0
            return
        self.age = (datetime.now() - datetime.strptime(self.registration, '%Y-%m-%dT%H:%M:%SZ')).days



def set_user_global_info(user: User):
    params = {
        'action': 'query',
        'meta': 'globaluserinfo',
        'guiuser': user.user_name,
        'guiprop': 'groups|editcount|merged',
    }
    res = call_commons_api(params)['query']['globaluserinfo']
    user.home_wiki = res['home']
    user.global_groups = ', '.join(res['groups'])
    user.globally_locked = 'locked' in res
    user.blocked_in_wikis = ', '.join([i['wiki'] for i in res['merged'] if 'blocked' in i])
    user.global_edit_count = sum([i['editcount'] for i in res['merged']])
    user.formatted_global_edit_count = '{:,}'.format(user.global_edit_count)


def build_user_library(users):
    users_library = {}

    params = {
        'action': 'query',
        'list': 'users',
        'ususers': '|'.join(users),
        'usprop': 'blockinfo|groups|editcount|registration',
    }
    res = call_commons_api(params)
    for i in res['query']['users']:
        user = User()
        user.user_name = i['name']
        user.edit_count = i['editcount']
        user.formatted_edit_count = '{:,}'.format(i['editcount'])
        user.blocked = 'blockid' in i
        user.groups = ', '.join(i['groups'])
        user.registration = i['registration']
        user.set_age_from_registration()
        set_user_global_info(user)
        users_library[i['name']] = user
    
    return users_library
    